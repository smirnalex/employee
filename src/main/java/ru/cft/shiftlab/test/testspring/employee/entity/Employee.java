package ru.cft.shiftlab.test.testspring.employee.entity;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class Employee {
    @NonNull
    private int id;
    @NonNull
    private String name;
    @NonNull
    private String surname;
    @NonNull
    private String patronymic;
    @NonNull
    private int departmentId;
    @NonNull
    private int positionId;
}
