package ru.cft.shiftlab.test.testspring.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.cft.shiftlab.test.testspring.employee.entity.Employee;
import ru.cft.shiftlab.test.testspring.employee.rowmapper.EmployeeRowMapper;

import java.sql.PreparedStatement;
import java.util.List;

@Repository
public class EmployeesRepository {

    private JdbcTemplate jdbcTemplate;
    private EmployeeRowMapper employeeRowMapper;

    @Autowired
    public EmployeesRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.employeeRowMapper = new EmployeeRowMapper();
    }

    public void updateEmployee(Employee newEmployee){
        String sql = "UPDATE Employees " +
                     "SET Name = ?, Surname = ?, Patronymic = ?, DepartmentID = ?, PositionID = ? " +
                     "WHERE EmployeeID = ?";
        Object[] params = {
                newEmployee.getName(),
                newEmployee.getSurname(),
                newEmployee.getPatronymic(),
                newEmployee.getDepartmentId(),
                newEmployee.getPositionId(),
                newEmployee.getId()
        };
        jdbcTemplate.query(sql, params, employeeRowMapper);
    }

    public void deleteEmployeeById(int id){
        String sql = "DELETE FROM Employees " +
                     "WHERE EmployeeID = ?";
        jdbcTemplate.query(sql, new Object[]{id}, employeeRowMapper);
    }

    public Employee getEmployeeById(int id){
        String sql = "SELECT * FROM Employees " +
                     "WHERE EmployeeID = ?";
        List<Employee> result = jdbcTemplate.query(sql, new Object[]{id}, employeeRowMapper);
        return result.isEmpty() ? null : result.get(0);
    }

    public List<Employee> getEmployers(){
        String sql = "SELECT * FROM Employees";
        return jdbcTemplate.query(sql, employeeRowMapper);
    }

    public int insertEmployee(Employee employee){
        String sql = "INSERT INTO EMPLOYEES " +
                     "(Name, Surname, Patronymic, DepartmentID, PositionID) " +
                     "VALUES(?, ?, ?, ?, ?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();

        PreparedStatementCreator statementCreator = connection -> {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, employee.getName());
            ps.setString(2, employee.getSurname());
            ps.setString(3, employee.getPatronymic());
            ps.setInt(4, employee.getDepartmentId());
            ps.setInt(5, employee.getPositionId());
            return ps;
        };

        jdbcTemplate.update(statementCreator, keyHolder);

        return (int)keyHolder.getKey();
    }
}
