package ru.cft.shiftlab.test.testspring.transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransactionController {

    private TransactionService transactionService;
    private AuthorisationService authorisationService;

    @Autowired
    public TransactionController(TransactionService transactionService, AuthorisationService authorisationService) {
        this.transactionService = transactionService;
        this.authorisationService = authorisationService;
    }

    @PostMapping("{login}/{password}/send/{count}/to/{receiver}")
    public ResponseEntity<String> send(@PathVariable String login,
                                       @PathVariable String password,
                                       @PathVariable int count,
                                       @PathVariable String receiver){

        if (!authorisationService.isAuthorised(login, password)){
            return new ResponseEntity<>("User is not authorized!", HttpStatus.UNAUTHORIZED);
        }

        try{
            transactionService.processTransaction(login, receiver, count);
        }
        catch (Exception e){
            return new ResponseEntity<>(e.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(null, HttpStatus.OK);
    }

}
