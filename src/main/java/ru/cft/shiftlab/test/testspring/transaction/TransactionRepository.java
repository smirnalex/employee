package ru.cft.shiftlab.test.testspring.transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class TransactionRepository {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public TransactionRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public boolean isUserExists(String userName){
        return true;
    }

    public int getBalance(String userName){
        return 100;
    }

    public void setBalance(String userName, int newBalance){

    }

}
