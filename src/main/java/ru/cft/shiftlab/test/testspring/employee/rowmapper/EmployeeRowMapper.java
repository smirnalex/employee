package ru.cft.shiftlab.test.testspring.employee.rowmapper;


import org.springframework.jdbc.core.RowMapper;
import ru.cft.shiftlab.test.testspring.employee.entity.Employee;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeRowMapper implements RowMapper<Employee> {

    @Override
    public Employee mapRow(ResultSet resultSet, int rowNum) throws SQLException {

        return Employee.builder()
                .id(resultSet.getInt("EmployeeID"))
                .name(resultSet.getString("Name"))
                .surname(resultSet.getString("Surname"))
                .patronymic(resultSet.getString("Patronymic"))
                .departmentId(resultSet.getInt("DepartmentID"))
                .positionId(resultSet.getInt("PositionID"))
                .build();
    }

}