package ru.cft.shiftlab.test.testspring.transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionService {

    private TransactionRepository transactionRepository;

    @Autowired
    public TransactionService(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public void processTransaction(String sender, String receiver, int count) throws Exception{
        if (!transactionRepository.isUserExists(receiver)) {
            throw new Exception("Receiver does not exist!");
        }

        int senderBalance = transactionRepository.getBalance(sender);
        int receiverBalance = transactionRepository.getBalance(receiver);

        if (senderBalance < count){
            throw new Exception("Sender has insufficient funds!");
        }

        senderBalance = senderBalance - count;
        transactionRepository.setBalance(sender, senderBalance);
        receiverBalance = receiverBalance + count;
        transactionRepository.setBalance(receiver, receiverBalance);
    }

}
