CREATE TABLE Employees (
    EmployeeID SERIAL NOT NULL PRIMARY KEY,
    Name VARCHAR(50) NOT NULL,
    Surname VARCHAR(50) NOT NULL,
    Patronymic VARCHAR(50) NOT NULL,
    DepartmentID INTEGER REFERENCES DEPARTMENTS(DepartmentID),
    PositionID INTEGER REFERENCES EmployeesPositions(PositionID)
);

CREATE TABLE Departments(
    DepartmentID SERIAL NOT NULL PRIMARY KEY,
    Address VARCHAR(100),
    Floor SMALLINT
);

CREATE TABLE DepartmentsHeads(
    DepartmentID INTEGER NOT NULL REFERENCES Departments(DepartmentID),
    HeadID INTEGER NOT NULL REFERENCES Employees(EmployeeID)
);

CREATE TABLE EmployeesPositions(
    PositionID SERIAL NOT NULL PRIMARY KEY,
    PositionName VARCHAR(50) NOT NULL,
    Salary INTEGER NOT NULL
);